FROM openjdk:11
WORKDIR /usr/src
COPY ./target/*.jar ./docker-compose-example.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "docker-compose-example.jar"]