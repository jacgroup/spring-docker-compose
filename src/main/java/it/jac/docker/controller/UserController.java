package it.jac.docker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.jac.docker.dto.UserDto;
import it.jac.docker.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/user")
	public List<UserDto> getAll () {
		return userService.getAllUsers();
	}
	
	@GetMapping("/user/{id}")
	public UserDto getById (@PathVariable Long id) {
		return userService.getUserById(id);
	}
	
	@PostMapping("/user")
	public UserDto save (@RequestBody UserDto user) {
		return userService.saveUser(user);
	}
	
	@PutMapping("/user")
	public UserDto update (@RequestBody UserDto user) {
		return userService.updateUser(user);
	}
	
	@DeleteMapping("/user/{id}")
	public void delete (@PathVariable Long id) {
		userService.deleteUser(id);
	}
	
}
