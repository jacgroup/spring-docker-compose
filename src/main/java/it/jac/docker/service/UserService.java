package it.jac.docker.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.jac.docker.dto.UserDto;
import it.jac.docker.entity.User;
import it.jac.docker.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ModelMapper modelMapper;

	
	public List<UserDto> getAllUsers () {
		return userRepository.findAll().stream()
						.map(u -> modelMapper.map(u, UserDto.class))
						.collect(Collectors.toList());
	}
	
	public UserDto getUserById (Long id) {
		Optional<User> user = userRepository.findById(id);
		if(user.isEmpty()) {
			return null;
		}
		
		return modelMapper.map(user.get(), UserDto.class);
	}
	
	public UserDto saveUser (UserDto userDto) {
		User user = modelMapper.map(userDto, User.class);
		
		return modelMapper.map(userRepository.save(user), UserDto.class);
	}
	
	public UserDto updateUser (UserDto userDto) {
		Optional<User> userOptional = userRepository.findById(userDto.getId());
		if(userOptional.isEmpty()) {
			throw new IllegalArgumentException ("User not found");
		}
		User user = userOptional.get();
		
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		
		User savedUser = userRepository.save(user);
		
		return modelMapper.map(savedUser, UserDto.class);
	}
	
	public void deleteUser (Long userId) {
		userRepository.deleteById(userId);
	}
}
