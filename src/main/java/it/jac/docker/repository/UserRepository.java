package it.jac.docker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.jac.docker.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
}
