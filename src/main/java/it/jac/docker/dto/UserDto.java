package it.jac.docker.dto;

import lombok.Data;

@Data
public class UserDto {
	
	private Long id;
	private String username;
	private String firstName;
	private String lastName;
	
	public String getFullName() {
		return firstName + " " + lastName;
	}
	
}
