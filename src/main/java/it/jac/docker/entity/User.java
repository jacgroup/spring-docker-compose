package it.jac.docker.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class User {
	@Id @GeneratedValue
	Long id;
	String username;
	String firstName;
	String lastName;
}
